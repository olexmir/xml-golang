package main

import (
	"encoding/xml"
	"fmt"
	"github.com/lestrrat-go/libxml2"
	"github.com/lestrrat-go/libxml2/xsd"
	"io/ioutil"
	"log"
	"encoding/json"
	"gopkg.in/xmlpath.v2"
	"bytes"
)

type BeerModel struct {
	Beers []struct {
		Id           string `xml:"id,attr"`
		Type         string `xml:"type,attr"`
		Name         string `xml:"name"`
		Manufacturer string `xml:"manufacturer"`
		Ingredients  struct {
			Water float32 `xml:"water"`
			Solod float32 `xml:"solod"`
			Khmel float32 `xml:"khmel"`
			Sugar float32 `xml:"sugar"`
		} `xml:"ingredients"`
		Chars struct {
			Turnovers float32 `xml:"turnovers"`
			Opacity   float32 `xml:"opacity"`
			Nutrition int     `xml:"nutrition"`
			Filter    bool    `xml:"filter"`
		} `xml:"chars"`
	} `xml:"beer"`
}

func validate(xmlContent, xsdContent []byte) bool {
	s, err := xsd.Parse(xsdContent)
	if err != nil {
		log.Printf("failed to parse XSD: %s", err)
		return false
	}
	defer s.Free()

	d, err := libxml2.Parse(xmlContent)
	if err != nil {
		log.Printf("failed to parse XML: %s", err)
		return false
	}

	if err := s.Validate(d); err != nil {
		for _, e := range err.(xsd.SchemaValidationError).Errors() {
			log.Printf("error: %s", e.Error())
		}
		return false
	}

	log.Printf("validation successful!")
	return true
}

func main() {
	xmlPath, xsdPath := "./beer.xml", "./beer.xsd"

	xsdContent, err := ioutil.ReadFile(xsdPath)
	if err != nil {
		log.Printf("failed to read file: %s %s", xsdPath, err)
	}

	xmlContent, err := ioutil.ReadFile(xmlPath)
	if err != nil {
		log.Printf("failed to read file: %s %s", xmlPath, err)
	}

	isValid := validate(xmlContent, xsdContent)

	if !isValid {
		return
	}
	fmt.Println(isValid)
	var beers BeerModel
	err1 := xml.Unmarshal(xmlContent, &beers)
	if err1 != nil {
		log.Printf("failed to convert to object: %s", err)
		return
	}

	fmt.Println("Beers:", beers.Beers)

	j, err := json.Marshal(&beers.Beers)
	if err != nil {
		log.Printf("failed to convert to json: %s", err)
		return
	}

	fmt.Println("JSON:", string(j))

	path := xmlpath.MustCompile(`/beers/beer[@id="2"]`)
	root, err := xmlpath.Parse(bytes.NewReader(xmlContent))
	if err != nil {
		log.Fatal(err)
	}
	if value, ok := path.String(root); ok {
		fmt.Println("Found:", value)
	}
}
